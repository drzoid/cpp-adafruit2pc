#include <Arduino.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>

#include <sprite.h>     //librairie sans les fixs
#include "NewSprite.h"  //librairie avec les try de fix (+5w)

#include <chrono>
#include <thread>

Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(32, 24, 1, 1, -1,
  NEO_MATRIX_TOP      + NEO_MATRIX_LEFT +
  NEO_MATRIX_ROWS     + NEO_MATRIX_PROGRESSIVE,
  NEO_RGB             + NEO_KHZ400);

class TestLibSprite1 : public Sprite {
  public:
    int x=33;
    TestLibSprite1(uint8_t width, uint8_t height, int16_t x, int16_t y, uint8_t fps) : Sprite(width, height, x, y, fps) {
      setState(0);
      setTextWrap(false);
    }

    bool draw() {
      fillScreen(Color(0,0,255));
      //drawPixel(0,0, Color(255,0,0));
			//drawPixel(31,0,  Color(0,255,0));
			//drawPixel(0,7, Color(0,0,255));
			//drawPixel(31,7,  Color(255,0,255));
      setCursor(x--,0);
      print("Connected, ip: 192.168.77.99");
      return true;
    }

};

class TestLibSprite2 : public NewSprite {
  public:
    int x=13;
    TestLibSprite2(uint8_t width, uint8_t height, int16_t x, int16_t y, uint8_t fps) : NewSprite(width, height, x, y, fps) {
      setState(0);
      setTextWrap(false);
    }

    bool draw() {
      fillScreen(Color(0,255,0));
      //matrix.setTextWrap(true); 
			//drawPixel(0,0, Color(255,0,0));
			//drawPixel(31,0,  Color(0,255,0));
			//drawPixel(0,7, Color(0,0,255));
			//drawPixel(31,7,  Color(255,0,255));
      setCursor(x--,0);
      print("Connected, ip: 192.168.77.99");
      return true;
    }

};

//pas ok. overlap de texte a la droite de l'ecran losque aqu'un caractere est a cheval a gauche
TestLibSprite1* testLibSprite1 = new TestLibSprite1(32,8,0,16,2);

//ok car taille augmentee de 5 pixel de large!
TestLibSprite2* testLibSprite2 = new TestLibSprite2(12,8,0,16,2);

void setup() {
  matrix.begin();
  matrix.setTextWrap(false);
  //matrix.print("Zilloscope");
  //matrix.drawPixel(0,0, Color(255,0,0));
	//matrix.drawPixel(31,0,  Color(0,255,0));
	//matrix.drawPixel(0,7, Color(0,0,255));
	//matrix.drawPixel(31,7,  Color(255,0,255));
  matrix.fillScreen(0);
  //matrix.drawRGBBitmap(0, 0, sprite_bitmap_panda,12, 8);
  matrix.show();
}

int x=33;
ulong lastUpdate=-500;
ulong lastShow=0;
bool drawing=false;

//Fonction "loop" arduino
void loop() {

  ulong timer = millis();

  if((timer-lastUpdate)>(1000/2)){
    matrix.fillRect(0,0,32,8, matrix.Color(255,0,0));
    //matrix.setTextWrap(true); 
    matrix.setCursor(x--,0);
    matrix.print("Connected, ip: 192.168.77.99");
    drawing=true; 
    lastUpdate=timer;
  }
   
  if(testLibSprite1->update(millis())){ 
    matrix.drawRGBBitmap(testLibSprite1->getLeft(),8,testLibSprite1->getBuffer(),testLibSprite1->getWidth(), testLibSprite1->getHeight());
    drawing=true;
  }  
  
  if(testLibSprite2->needScreenUpdate(millis())){ 
    matrix.drawRGBBitmap(testLibSprite2->getLeft(),16,testLibSprite2->getBuffer(),testLibSprite2->getWidth(), testLibSprite2->getHeight());
    drawing=true;
  }

  if(drawing){
    matrix.show();
    lastShow=millis();
    drawing=false;
  }
}
