#include "spritemanager.h"

#include "spritesheet_8x12_static.h"

SpriteManager::SpriteManager(Adafruit_NeoMatrix* matrix) {

    _matrix=matrix;
     
    //Instanciation des sprites
    blinkSprite1 = new BlinkSprite(3/*width*/, 3/*heigth*/,27/*x*/,5/*y*/, 1/*fps*/);
    //blinkSprite2 = new BlinkSprite(4/*width*/, 4/*heigth*/,22/*x*/,4/*y*/, 2/*fps*/);
    //blinkSprite3 = new BlinkSprite(1/*width*/, 1/*heigth*/,31/*x*/,7/*y*/, 30/*fps*/);
    //rainbowSprite = new RainbowSprite(5/*width*/, 5/*heigth*/,16/*x*/,3/*y*/, 24/*fps*/);
    //animSprite1 = new AnimSprite(  5/*width*/, 5/*heigth*/,16/*x*/,3/*y*/, 24/*fps*/);
    //animSprite2 = new AnimSprite(5/*width*/, 5/*heigth*/,16/*x*/,3/*y*/, 24/*fps*/);
    staticSprite1 = new StaticSprite(sprite_bitmap_panda, 0, 12/*width*/, 8/*heigth*/, 13/*x*/, 0/*y*/);
    //staticSprite2 = new StaticSprite(sprite_bitmap_test, 0, 1/*width*/, 1/*heigth*/, 0/*x*/, 0/*y*/);
}

bool SpriteManager::update() {
    
    unsigned long timer = millis();
    
    bool needMatrixUpdate = false;

    //update de tous les sprites instancies en leur passant le delta.
    //retourne true si besoin de mettre a jour la matrice. (si un seul des sprite change)
    //TODO: boucle sur un tableau de sprite?
    needMatrixUpdate = needMatrixUpdate || blinkSprite1->update(timer);
    //needMatrixUpdate = needMatrixUpdate || blinkSprite2->update(timer);
    //needMatrixUpdate = needMatrixUpdate || blinkSprite3->update(timer);
    //needMatrixUpdate = needMatrixUpdate || rainbowSprite->update(timer);
    //needMatrixUpdate = needMatrixUpdate || animSprite1->update(timer);
    //needMatrixUpdate = needMatrixUpdate || animSprite2->update(timer);

    needMatrixUpdate = needMatrixUpdate || staticSprite1->update(timer);
    //needMatrixUpdate = needMatrixUpdate || staticSprite2->update(timer);

    //mis a jour buffer matrice
    if(needMatrixUpdate)
        drawMatrix();
    
    return needMatrixUpdate;
}

// Draw a PROGMEM-resident 16-bit image (RGB 5/6/5) at the specified (x,y)
// position.  For 16-bit display devices; no color reduction performed.
void SpriteManager::drawRGBBitmap(int16_t x, int16_t y, uint16_t bitmap[], int16_t w, int16_t h) {
    _matrix->startWrite();
    for(int16_t py=0; py<h; py++) {
        for(int16_t px=0; px<w; px++) {
            _matrix->writePixel(px+x, py+y, pgm_read_word(&bitmap[(py*(w)) + px]));
        }
    }
    _matrix->endWrite();
}


void SpriteManager::drawMatrix() {
    
    //dessine le buffer de chaque sprite au bon endroit sur la matrice
    
    //TODO: boucle sur un tableau de sprite?
    drawRGBBitmap(blinkSprite1->getLeft(),blinkSprite1->getTop(),blinkSprite1->getBuffer(),blinkSprite1->getWidth(), blinkSprite1->getHeight());
    //_matrix->drawRGBBitmap(blinkSprite2->getLeft(),blinkSprite2->getTop(),blinkSprite2->getBuffer(),blinkSprite2->getWidth(), blinkSprite2->getHeight());
    //_matrix->drawRGBBitmap(blinkSprite3->getLeft(),blinkSprite3->getTop(),blinkSprite3->getBuffer(),blinkSprite3->getWidth(), blinkSprite3->getHeight());
    //_matrix->drawRGBBitmap(rainbowSprite->getLeft(),rainbowSprite->getTop(),rainbowSprite->getBuffer(),rainbowSprite->getWidth(), rainbowSprite->getHeight());
    //_matrix->drawRGBBitmap(animSprite1->getLeft(),animSprite1->getTop(),animSprite1->getBuffer(),animSprite1->getWidth(), animSprite1->getHeight());
    //_matrix->drawRGBBitmap(animSprite2->getLeft(),animSprite2->getTop(),animSprite2->getBuffer(),animSprite2->getWidth(), animSprite2->getHeight());

    drawRGBBitmap(staticSprite1->getLeft(),staticSprite1->getTop(),staticSprite1->getBuffer(),staticSprite1->getWidth(), staticSprite1->getHeight());
    //_matrix->drawRGBBitmap(staticSprite2->getLeft(),staticSprite2->getTop(),staticSprite2->getBuffer(),staticSprite2->getWidth(), staticSprite2->getHeight());
}