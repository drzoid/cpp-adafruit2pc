#include "blinksprite.h"

BlinkSprite::BlinkSprite(uint8_t width, uint8_t height, int16_t x, int16_t y, uint8_t fps) : Sprite(width, height, x, y, fps) {
    setState(0);
}

bool BlinkSprite::draw() {
    setState(!getState());
    if(getState())
        fillRect(0,0, WIDTH, HEIGHT, Color(255, 0, 0));
    else
        fillRect(0,0, WIDTH, HEIGHT, Color(255, 255, 255));
    return true;
}
