#include "NewSprite.h"

NewSprite::NewSprite( uint8_t width, uint8_t height, int16_t x, int16_t y, uint8_t fps) : Adafruit_GFX(width, height) {
	_fps=fps;
	_x=x;
	_y=y;
	_w=width;
	_h=height;
	uint16_t bytes = (((width) * height)+1) * 2;
	if((_buffer = (uint16_t *)malloc(bytes))) {
		memset(_buffer, 0, bytes+1);
	}
	fillScreen(Color(255,0,0));
}

NewSprite::~NewSprite(void) {
	if(_buffer) free(_buffer);
}

uint16_t* NewSprite::getBuffer(void) {
	return _buffer;
}

// Downgrade 24-bit color to 16-bit (add reverse gamma lookup here?)
uint16_t NewSprite::Color(uint8_t r, uint8_t g, uint8_t b) {
  return ((uint16_t)(r & 0xF8) << 8) |
		 ((uint16_t)(g & 0xFC) << 3) |
					(b         >> 3);
}

uint8_t NewSprite::getState(){
	return _state;
}

void NewSprite::setFPS(uint8_t fps){
	_fps=fps;
}

void NewSprite::setState(uint8_t state){
	_state=state;
}

uint8_t NewSprite::getHeight(){
	return _h;
}

uint8_t NewSprite::getWidth(){
	return WIDTH;
}

int16_t NewSprite::getLeft(){
	return _x;
}

int16_t NewSprite::getTop(){
	return _y;
}

bool NewSprite::needScreenUpdate(unsigned long timer) {

	unsigned long delta = timer - _lastUpdate;

	if(delta<(1000/_fps))  {
		return false;
	}

	bool changes=draw();
	_lastUpdate=timer; // sauvegarde du timer pour le prochain delta temps
	return changes;
}

//TODO: optimisation des autres fonctions: fillrect, drawline, etc
void NewSprite::drawPixel(int16_t x, int16_t y, uint16_t color) {
	//dessine le pixel ds le buffer du sprite
	if(x>=_w || x<0) return;
	if(y>=_h || y<0) return;
	
	_buffer[((WIDTH)*y)+x]=color;
}
