#ifndef Sprite_Manager_h
#define Sprite_Manager_h


#include <Adafruit_NeoMatrix.h>

#include "sprite.h"
#include "blinksprite.h"
#include "rainbowsprite.h"
#include "animsprite.h"
#include "staticsprite.h"




class SpriteManager
{
  public:

    SpriteManager(Adafruit_NeoMatrix* matrix);

    void drawMatrix();
    bool update();

    BlinkSprite* blinkSprite1;
    BlinkSprite* blinkSprite2; 
    BlinkSprite* blinkSprite3;

    RainbowSprite* rainbowSprite;

    AnimSprite* animSprite1;
    AnimSprite* animSprite2;
    
    StaticSprite* staticSprite1;
    StaticSprite* staticSprite2;
 
    void drawRGBBitmap(int16_t x, int16_t y,  uint16_t bitmap[], int16_t w, int16_t h);

  protected:
    Adafruit_NeoMatrix* _matrix;
};

#endif