#ifndef _Z_MODULE_WIFI_H
#define _Z_MODULE_WIFI_H

#include "NewSprite.h"

#define ZWIFI_ANIM_NONE 0
#define ZWIFI_ANIM_CONNECTING 1
#define ZWIFI_ANIM_CONNECTED 2
#define ZWIFI_ANIM_DISCONNECTED 3

static const uint16_t image_data_wifi_on[96] = {
    // ∙∙∙▒▒▒▒▒▒∙∙∙
    // ∙∙▒∙∙∙∙∙∙▒∙∙
    // ∙▒∙∙▒▒▒▒∙∙▒∙
    // ∙∙∙▒∙∙∙∙▒∙∙∙
    // ∙∙∙∙∙▒▒∙∙∙∙∙
    // ∙∙∙∙▒▒▒▒∙∙∙∙
    // ∙∙∙∙∙▒▒∙∙∙∙∙
    // ∙∙∙∙∙∙∙∙∙∙∙∙
    0x0000, 0x0000, 0x0000, 0x07e4, 0x07e4, 0x07e4, 0x07e4, 0x07e4, 0x07e4, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x07e4, 0x0900, 0x0900, 0x0900, 0x0900, 0x0900, 0x0900, 0x07e4, 0x0000, 0x0000, 
    0x0000, 0x07e4, 0x0000, 0x0000, 0x07e4, 0x07e4, 0x07e4, 0x07e4, 0x0000, 0x0000, 0x07e4, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x07e4, 0x0000, 0x0000, 0x0000, 0x0000, 0x07e4, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x07e4, 0x07e4, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x07e4, 0x07e4, 0x07e4, 0x07e4, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x07e4, 0x07e4, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
};

//affichage
class WifiFullscreenSprite : public NewSprite {
public:

	WifiFullscreenSprite() : NewSprite(32, 8, 0, 8, 2) {
		setTextWrap(false);
		setState(ZWIFI_ANIM_NONE);
		setFont();
	}

	void showWifiConnected(const char * ip) {
		sprintf(_message, "Connected, IP: %s", ip);
		_pass=0;
		_x=getWidth(); //scroll depuis la droite de la matrice
		setFPS(12);
		setState(ZWIFI_ANIM_CONNECTED);
	}

	void showWifiDisconnected() {
		sprintf(_message, "Wifi disconnected");
		_pass=0;
		_x=getWidth(); //scroll depuis la droite de la matrice
		setFPS(12);
		setState(ZWIFI_ANIM_DISCONNECTED);
	}

	void showConnecting() {
		setTextWrap(true);
		_pass=0;
		_x=-15; //scroll depuis la gauche de la matrice (...)
		setFPS(24);
		setState(ZWIFI_ANIM_CONNECTING);
	}

	void incrementProgress() {
		if(_counter++>100)
			_counter=0;
	}

	void resetProgress() {
	 	_counter=0;
	}

	bool draw() {
	
		uint8_t state=getState();

		if(state==ZWIFI_ANIM_CONNECTING){
			fillScreen(0);
			setCursor(_x, -2);
			setTextColor(Color(0,0,255));
			print("...");
			if(++_x > getWidth()) {
				_x = -15;
			}
		}
		else if(state==ZWIFI_ANIM_CONNECTED){

			if(_pass==5){
				fillScreen(0);
				setState(ZWIFI_ANIM_NONE);
				return true;
			} 
			
			fillScreen(0);
			setCursor(_x, 0);
			setTextColor(Color(0,255,255));
			print(_message);

			drawRGBBitmap(0,0,image_data_wifi_on,12,8);

			if(_pass>0) {
				_pass++;
				return true;
			}

			uint16_t bw,bh;
			int16_t bx,by;
			getTextBounds(_message, 0, 0, &bx, &by, &bw, &bh);
			if(--_x < (static_cast<int>(getWidth())-static_cast<int>(bw))) {
				_pass++;
				setFPS(1);
			}
		}
 		else if(state==ZWIFI_ANIM_DISCONNECTED){
			fillScreen(0);
			setCursor(_x, 0);
			setTextColor(Color(0,0,255));
			print(_message);
			uint16_t bw,bh;
			int16_t bx,by;
			getTextBounds(_message, 0, 0, &bx, &by, &bw, &bh);
			//std::cerr << "_x=" << (int)_x << ", w=" << (int)bw << ", pass=" << (int)_pass << "\n";
			if(--_x < (static_cast<int>(getWidth())-static_cast<int>(bw))) {
				_x=getWidth();
				_pass++;
			}
		}
		else {
			return false;
		}
		return true;
	}

private:
	char _message[255];
	uint8_t _counter=0;
	uint8_t _pass=0;
	int16_t _x=0;
};

#endif