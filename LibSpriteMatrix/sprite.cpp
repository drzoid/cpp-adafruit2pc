#include "sprite.h"

Sprite::Sprite( uint8_t width, uint8_t height, int16_t x, int16_t y, uint8_t fps) : Adafruit_GFX(width, height) {
    _fps=fps;
    _x=x;
    _y=y;
    uint32_t bytes = ((width * height)+1) * 2;
    if((_buffer = (uint16_t *)malloc(bytes))) {
        memset(_buffer, 0, bytes);
    }
    setTextWrap(false);
    //fillScreen(0);
}

Sprite::~Sprite(void) {
    if(_buffer) free(_buffer);
}

uint16_t* Sprite::getBuffer(void) {
    return _buffer;
}

// Downgrade 24-bit color to 16-bit (add reverse gamma lookup here?)
uint16_t Sprite::Color(uint8_t r, uint8_t g, uint8_t b) {
  return ((uint16_t)(r & 0xF8) << 8) |
         ((uint16_t)(g & 0xFC) << 3) |
                    (b         >> 3);
}

uint8_t Sprite::getState(){
    return _state;
}

uint8_t Sprite::getHeight(){
    return HEIGHT;
}

uint8_t Sprite::getWidth(){
    return WIDTH;
}

int16_t Sprite::getLeft(){
    return _x;
}

int16_t Sprite::getTop(){
    return _y;
}

void Sprite::setState(uint8_t state){
    _state=state;
}

bool Sprite::update(unsigned long timer) {

    unsigned long delta = timer - _lastUpdate;

    if(delta<(1000/_fps))  {
        return false;
    }

    bool changes=draw();
    _lastUpdate=timer; // sauvegarde du timer pour le prochain delta temps
    return changes;
}


//TODO: optimisation des autres fonctions: fillrect, drawline, etc
void Sprite::drawPixel(int16_t x, int16_t y, uint16_t color) {
    //dessine le pixel ds le buffer du sprite
    if(x>=WIDTH) return;
	if(y>=HEIGHT) return;
	_buffer[((WIDTH)*y)+x]=color;
}

